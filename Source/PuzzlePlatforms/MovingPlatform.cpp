// Fill out your copyright notice in the Description page of Project Settings.


#include "MovingPlatform.h"

AMovingPlatform::AMovingPlatform() {
	PrimaryActorTick.bCanEverTick = true;
	SetMobility(EComponentMobility::Movable);
}

void AMovingPlatform::AddActiveTrigger() {
	ActiveTriggers++;
}

void AMovingPlatform::RemoveActiveTrigger() {
	ActiveTriggers--;
}

void AMovingPlatform::Tick(float DeltaSeconds) {
	Super::Tick(DeltaSeconds);
	if (HasAuthority()) {
		if (ActiveTriggers > 0) {
			if (!Reverse) {
				FVector Location = GetActorLocation();
				Location += Speed * DeltaSeconds * (GlobalTargetLocation - GlobalStartLocation).GetSafeNormal();
				SetActorLocation(Location);
				const float JourneyLength = (GlobalTargetLocation - GlobalStartLocation).Size();
				const float JourneyTravelled = (Location - GlobalStartLocation).Size();
				if (JourneyTravelled >= JourneyLength) Reverse = !Reverse;
			}
			else if (Reverse) {
				FVector Location = GetActorLocation();
				Location -= Speed * DeltaSeconds * (GlobalTargetLocation - GlobalStartLocation).GetSafeNormal();
				SetActorLocation(Location);
				const float JourneyLength = (GlobalStartLocation - GlobalTargetLocation).Size();
				const float JourneyTravelled = (Location - GlobalTargetLocation).Size();
				if (JourneyTravelled >= JourneyLength) Reverse = !Reverse;
			}
		}
	}
}

void AMovingPlatform::BeginPlay() {
	Super::BeginPlay();
	if (HasAuthority()) {
		SetReplicates(true);
		SetReplicateMovement(true);
	}
	GlobalStartLocation = GetActorLocation();
	GlobalTargetLocation = GetTransform().TransformPosition(TargetLocation);
}
