#include "PuzzlePlatformsGameInstance.h"

#include <Kismet/GameplayStatics.h>


#include "OnlineSessionSettings.h"
#include "OnlineSubsystem.h"
#include "PlatformTrigger.h"
#include "Blueprint/UserWidget.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "MenuSystem/MainMenu.h"
#include "MenuSystem/OverlayMenu.h"
#include "UObject/ConstructorHelpers.h"
#include "UObject/NameTypes.h"

const static FName SESSION_NAME = TEXT("GameSession");
const static auto SERVER_NAME_SETTINGS_KEY = TEXT("ServerName");

void UPuzzlePlatformsGameInstance::Init() {
	Super::Init();
	OnlineSubsystem = IOnlineSubsystem::Get();
	if (!ensure(OnlineSubsystem!=nullptr)) return;
	SessionInterface = OnlineSubsystem->GetSessionInterface();
	if (!SessionInterface.IsValid()) return;
	SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(
		this, &UPuzzlePlatformsGameInstance::OnCreateSessionComplete);
	SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(
		this, &UPuzzlePlatformsGameInstance::OnDestroySessionComplete);
	SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(
		this, &UPuzzlePlatformsGameInstance::OnFindSessionsComplete);
	SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(
		this, &UPuzzlePlatformsGameInstance::OnJoinSessionComplete);

	SessionSearch = MakeShareable(new FOnlineSessionSearch());
	if (SessionSearch.IsValid()) {
		SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
	}

	if (GEngine != nullptr) {
		GEngine->OnNetworkFailure().AddUObject(this, &UPuzzlePlatformsGameInstance::OnNetworkFailure);
	}
}

void UPuzzlePlatformsGameInstance::Host(FString ServerName) {
	DesiredServerName = ServerName;
	if (SessionInterface.IsValid()) {
		const auto ExistingSession = SessionInterface->GetNamedSession(SESSION_NAME);
		if (ExistingSession != nullptr) {
			SessionInterface->DestroySession(SESSION_NAME);
		}
		else {
			CreateSession();
		}
	}
}

void UPuzzlePlatformsGameInstance::OnDestroySessionComplete(const FName Name, const bool bSuccess) {
	if (bSuccess) {
		CreateSession();
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Previous game session was not successfully destroyed"));
	}
}

void UPuzzlePlatformsGameInstance::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result) {
	if (!SessionInterface.IsValid()) return;
	FString Address;
	if (!SessionInterface->GetResolvedConnectString(SessionName, Address)) {
		UE_LOG(LogTemp, Warning, TEXT("Could not get connect string"));
		return;
	}
	UEngine* Engine = GetEngine();
	if (!ensure(Engine != nullptr)) return;

	auto PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController!=nullptr)) return;

	PlayerController->ClientTravel(Address, ETravelType::TRAVEL_Absolute);
}

void UPuzzlePlatformsGameInstance::OnNetworkFailure(UWorld* World, UNetDriver* Driver,
                                                    ENetworkFailure::Type FailureType, const FString& ErrorString) {
	LoadMainMenu();
}

void UPuzzlePlatformsGameInstance::CreateSession() const {
	if (SessionInterface.IsValid()) {
		FOnlineSessionSettings SessionSettings;
		if (IOnlineSubsystem::Get()->GetSubsystemName() == "NULL") {
			UE_LOG(LogTemp, Warning, TEXT("Starting in LAN mode"));
			SessionSettings.bIsLANMatch = true;
		}
		else {
			SessionSettings.bIsLANMatch = false;
		}
		SessionSettings.NumPublicConnections = 5;
		SessionSettings.bShouldAdvertise = true;
		SessionSettings.bUsesPresence = true;
		SessionSettings.Set(SERVER_NAME_SETTINGS_KEY, DesiredServerName,
		                    EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);

		SessionInterface->CreateSession(0, SESSION_NAME, SessionSettings);
	}
}

void UPuzzlePlatformsGameInstance::OnNetworkFailure() { }

void UPuzzlePlatformsGameInstance::OnFindSessionsComplete(bool bSuccess) {
	if (bSuccess && SessionSearch.IsValid() && MainMenu != nullptr) {
		TArray<FServerData> ServerNames;
		for (const FOnlineSessionSearchResult& Result : SessionSearch->SearchResults) {
			FServerData Data;
			FString ServerName;
			Data.MaxPlayers = Result.Session.SessionSettings.NumPublicConnections;
			Data.CurrentPlayers = Data.MaxPlayers - Result.Session.NumOpenPublicConnections;
			Data.HostUsername = Result.Session.OwningUserName;
			Data.Ping = Result.PingInMs;
			if (Result.Session.SessionSettings.Get(SERVER_NAME_SETTINGS_KEY, ServerName)) {
				Data.Name = ServerName;
			}
			else {
				Data.Name = Result.GetSessionIdStr();
				UE_LOG(LogTemp, Warning, TEXT("Could not find server name"));
			}
			ServerNames.Add(Data);
		}
		MainMenu->SetServerList(ServerNames);
	}
	else {
		UE_LOG(LogTemp, Display, TEXT("Session not found"));
	}
}

void UPuzzlePlatformsGameInstance::OnCreateSessionComplete(FName Name, const bool bSuccess) {
	if (bSuccess) {
		UEngine* Engine = GetEngine();
		if (!ensure(Engine!=nullptr)) return;
		Engine->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("Hosting"));
		UWorld* World = GetWorld();
		if (!ensure(World!=nullptr)) return;
		UGameplayStatics::OpenLevel(World, "Lobby", true, "listen");
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Failure creating online session"));
	}
}

void UPuzzlePlatformsGameInstance::Join(uint32 Index) {
	if (!SessionInterface.IsValid()) return;

	if (MainMenu != nullptr) {
		MainMenu->Teardown();
	}

	SessionInterface->JoinSession(0, SESSION_NAME, SessionSearch->SearchResults[Index]);
}

void UPuzzlePlatformsGameInstance::RefreshServerList() {
	SessionSearch = MakeShareable(new FOnlineSessionSearch());
	if (SessionSearch.IsValid()) {
		SessionSearch->MaxSearchResults = 10000;
		SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
		SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
	}
}

void UPuzzlePlatformsGameInstance::QuitToMainMenu() {
	UEngine* Engine = GetEngine();
	if (!Engine) return;
	APlayerController* PlayerController = GetFirstLocalPlayerController(GetWorld());
	if (!PlayerController) return;
	Engine->AddOnScreenDebugMessage(
		0,
		2,
		FColor::Green,
		FString::Printf(TEXT("Returning to Main Menu")));
	PlayerController->ClientTravel("/Game/MenuSystem/MainMenu", TRAVEL_Absolute);
}

void UPuzzlePlatformsGameInstance::QuitToDesktop() {
	RequestEngineExit(TEXT("Player clicked quit from ingame menu"));
}

UPuzzlePlatformsGameInstance::UPuzzlePlatformsGameInstance(const FObjectInitializer& ObjectInitializer) {
	static ConstructorHelpers::FClassFinder<APlatformTrigger> PlatformTriggerBPClass(
		TEXT("/Game/PuzzlePlatforms/BP_PlatformTrigger"));
	if (!ensure(PlatformTriggerBPClass.Class != nullptr)) return;

	static ConstructorHelpers::FClassFinder<UUserWidget> MainMenuBPClass(TEXT("/Game/MenuSystem/WBP_MainMenu"));
	if (!ensure(MainMenuBPClass.Class != nullptr)) return;
	MainMenuClass = MainMenuBPClass.Class;

	static ConstructorHelpers::FClassFinder<UUserWidget> OverlayMenuBPClass(
		TEXT("/Game/MenuSystem/WBP_OverlayMenu"));
	if (!ensure(OverlayMenuBPClass.Class != nullptr)) return;
	OverlayMenuClass = OverlayMenuBPClass.Class;
}

void UPuzzlePlatformsGameInstance::StartSession() const {
	if (SessionInterface.IsValid()) {
		SessionInterface->StartSession(SESSION_NAME);
	}
}

void UPuzzlePlatformsGameInstance::LoadMainMenu() {
	if (!ensure(MainMenuClass!=nullptr)) return;
	MainMenu = CreateWidget<UMainMenu>(this, MainMenuClass);
	if (!ensure(MainMenu!=nullptr)) return;
	MainMenu->Setup();
	MainMenu->SetMenuInterface(this);
}

void UPuzzlePlatformsGameInstance::LoadOverlayMenu() {
	if (!ensure(OverlayMenuClass!=nullptr))return;
	OverlayMenu = CreateWidget<UOverlayMenu>(this, OverlayMenuClass);
	if (!ensure(OverlayMenu!=nullptr))return;
	OverlayMenu->SetMenuInterface(this);
	OverlayMenu->Setup();
}
