// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "MovingPlatform.generated.h"

/**
 * 
 */
UCLASS()
class PUZZLEPLATFORMS_API AMovingPlatform : public AStaticMeshActor {
	GENERATED_BODY()
public:
	AMovingPlatform();
	void AddActiveTrigger();
	void RemoveActiveTrigger();
protected:
	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere)
	float Speed;
	UPROPERTY(EditAnywhere, Meta = (MakeEditWidget = true))
	FVector TargetLocation;
private:
	FVector GlobalTargetLocation;
	FVector GlobalStartLocation;
	bool Reverse = false;
	UPROPERTY(EditAnywhere)
	int32 ActiveTriggers = 1;
};
