#include "MainMenu.h"

#include <Components/EditableText.h>

#include "MainMenuInterface.h"
#include "ServerRow.h"
#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"

UMainMenu::UMainMenu(const FObjectInitializer& ObjectInitializer) : UUserWidget(ObjectInitializer) {
	ConstructorHelpers::FClassFinder<UUserWidget> ServerRowBPClass(TEXT("/Game/MenuSystem/WBP_ServerRow1"));
	if (!ensure(ServerRowBPClass.Class!=nullptr))return;
	ServerRowClass = ServerRowBPClass.Class;
}

bool UMainMenu::Initialize() {
	if (!Super::Initialize()) return false;
	if (!ensure(HostMenuButton!=nullptr)) return false;
	if (!ensure(JoinMenuButton!=nullptr)) return false;
	if (!ensure(MenuSwitcher!=nullptr))return false;
	if (!ensure(JoinCancelButton!=nullptr))return false;
	if (!ensure(HostCancelButton!=nullptr))return false;
	if (!ensure(RefreshButton!=nullptr))return false;
	if (!ensure(JoinGameButton!=nullptr))return false;
	if (!ensure(HostGameButton!=nullptr))return false;
	HostGameButton->OnClicked.AddDynamic(this, &UMainMenu::HostGameButtonClicked);
	HostMenuButton->OnClicked.AddDynamic(this, &UMainMenu::HostMenuButtonClicked);
	JoinMenuButton->OnClicked.AddDynamic(this, &UMainMenu::JoinMenuButtonClicked);
	JoinCancelButton->OnClicked.AddDynamic(this, &UMainMenu::CancelClicked);
	HostCancelButton->OnClicked.AddDynamic(this, &UMainMenu::CancelClicked);
	JoinGameButton->OnClicked.AddDynamic(this, &UMainMenu::JoinServer);
	RefreshButton->OnClicked.AddDynamic(this, &UMainMenu::RefreshClicked);
	return true;
}

void UMainMenu::SetMenuInterface(IMainMenuInterface* Interface) {
	this->MainMenuInterface = Interface;
}

/** Add widget to viewport 
 Set input mode on player controller and show mouse cursor */
void UMainMenu::Setup() {
	this->AddToViewport();
	UWorld* World = GetWorld();
	if (!ensure(World!=nullptr)) return;
	APlayerController* PlayerController = World->GetFirstPlayerController();
	if (!ensure(PlayerController!=nullptr)) return;
	FInputModeUIOnly InputModeData;
	InputModeData.SetWidgetToFocus(this->TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
	PlayerController->SetInputMode(InputModeData);
	PlayerController->bShowMouseCursor = true;
}

/** Remove widget from viewport 
 Set input mode on player controller and hide mouse cursor */
void UMainMenu::OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) {
	this->RemoveFromViewport();
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (!ensure(PlayerController!=nullptr))return;
	PlayerController->SetInputMode(FInputModeGameOnly{});
	PlayerController->bShowMouseCursor = false;
}

void UMainMenu::Teardown() {
	OnLevelRemovedFromWorld(nullptr, nullptr);
}

void UMainMenu::SetServerList(TArray<FServerData> ServerNames) {
	if (ServerNames.Num() == 0) return;
	UWorld* World = this->GetWorld();
	if (!ensure(World != nullptr)) return;
	if (ServerListScrollBox != nullptr) ServerListScrollBox->ClearChildren();
	uint32 RowCount = 0;
	for (const FServerData& ServerData : ServerNames) {
		UServerRow* Row = CreateWidget<UServerRow>(World, ServerRowClass);
		if (!ensure(Row!=nullptr))return;
		Row->ServerName->SetText(FText::FromString(ServerData.Name));
		Row->HostUsername->SetText(FText::FromString(ServerData.HostUsername));
		Row->ConnectionFraction->SetText(
			FText::FromString(FString::Printf(TEXT("%d / %d"), ServerData.CurrentPlayers, ServerData.MaxPlayers)));
		Row->Ping->SetText(FText::FromString(FString::Printf(TEXT("%d"), ServerData.Ping)));
		Row->Setup(this, RowCount++);
		ServerListScrollBox->AddChild(Row);
	}
}

void UMainMenu::UpdateChildren() {
	for (int32 i = 0; i < ServerListScrollBox->GetChildrenCount(); ++i) {
		auto Row = Cast<UServerRow>(ServerListScrollBox->GetChildAt(i));
		if (Row != nullptr) Row->Selected = (SelectedIndex.IsSet() && SelectedIndex.GetValue() == i);
	}
}

void UMainMenu::SelectIndex(const uint32 Index) {
	SelectedIndex = Index;
	UpdateChildren();
}

/** Use interface to host game */
void UMainMenu::HostMenuButtonClicked() {
	if (!ensure(MainMenuInterface!=nullptr)) return;
	if (!ensure(HostMenu != nullptr)) return;
	MenuSwitcher->SetActiveWidget(HostMenu);
}

void UMainMenu::HostGameButtonClicked() {
	MainMenuInterface->Host(HostServerName->GetText().ToString());
}

/** Use interface to join game */
void UMainMenu::JoinMenuButtonClicked() {
	if (!ensure(MenuSwitcher!=nullptr))return;
	if (!ensure(JoinMenu != nullptr))return;
	MenuSwitcher->SetActiveWidget(JoinMenu);
	RefreshClicked();
}

/** Use interface to join game */
void UMainMenu::JoinServer() {
	if (SelectedIndex.IsSet() && MainMenuInterface != nullptr) {
		UE_LOG(LogTemp, Display, TEXT("Selected index: %d"), SelectedIndex.GetValue());
		MainMenuInterface->Join(SelectedIndex.GetValue());
	}
	else {
		UE_LOG(LogTemp, Display, TEXT("Selected index not set"));
	}
}

/** Go back to main menu */
void UMainMenu::CancelClicked() {
	if (!ensure(MenuSwitcher!=nullptr))return;
	if (!ensure(MainMenu!=nullptr))return;
	MenuSwitcher->SetActiveWidget(MainMenu);
}

void UMainMenu::RefreshClicked() {
	if (MainMenuInterface != nullptr) {
		MainMenuInterface->RefreshServerList();
	}
}
