// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Blueprint/UserWidget.h"

#include "ServerRow.generated.h"

UCLASS()
class PUZZLEPLATFORMS_API UServerRow : public UUserWidget
{
public:
	GENERATED_BODY()
	void Setup(class UMainMenu* Parent, uint32 SetupIndex);

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* ServerName;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* HostUsername;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* Ping;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* ConnectionFraction;

	UPROPERTY(BlueprintReadOnly)
	bool Selected = false;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool bIsHeader = false;
protected:
	virtual bool Initialize() override;
private:
	UMainMenu* MainMenu;
	uint32 Index;

	UPROPERTY(meta = (BindWidget))
	class UButton* RowButton;

	UFUNCTION()
	void RowSelected();
};
