// Fill out your copyright notice in the Description page of Project Settings.


#include "OverlayMenu.h"
#include "Components/Button.h"
#include "GenericPlatform/GenericPlatformMisc.h"
#include "PuzzlePlatforms/PuzzlePlatformsGameInstance.h"

void UOverlayMenu::SetMenuInterface(IOverlayMenuInterface* MenuInterface)
{
    this->OverlayMenuInterface = MenuInterface;
}

void UOverlayMenu::Setup()
{
    if (!ensure(this != nullptr))return;
    this->AddToViewport();
    UWorld* World = GetWorld();
    if (!ensure(World != nullptr))return;
    APlayerController* PlayerController = World->GetFirstPlayerController();
    if (!ensure(PlayerController != nullptr))return;
    FInputModeUIOnly InputModeUIOnly;
    InputModeUIOnly.SetWidgetToFocus(this->TakeWidget());
    InputModeUIOnly.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
    PlayerController->SetInputMode(InputModeUIOnly);
    PlayerController->bShowMouseCursor = true;
}

void UOverlayMenu::OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)
{
    CloseMenu();
}

bool UOverlayMenu::Initialize()
{
    if (!Super::Initialize()) return false;
    if (!ensure(CloseMenuButton!=nullptr)) return false;
    if (!ensure(QuitToMenuButton!=nullptr)) return false;
    if (!ensure(QuitToDesktopButton!=nullptr)) return false;
    CloseMenuButton->OnClicked.AddDynamic(this, &UOverlayMenu::CloseClicked);
    QuitToMenuButton->OnClicked.AddDynamic(this, &UOverlayMenu::QuitToMenuClicked);
    QuitToDesktopButton->OnClicked.AddDynamic(this, &UOverlayMenu::QuitToDesktopClicked);
    return true;
}

void UOverlayMenu::CloseClicked()
{
    CloseMenu();
}

void UOverlayMenu::QuitToMenuClicked()
{
    OverlayMenuInterface->QuitToMainMenu();
}

void UOverlayMenu::QuitToDesktopClicked()
{
    OverlayMenuInterface->QuitToDesktop();
}

void UOverlayMenu::CloseMenu()
{
    if (!ensure(this != nullptr))return;
    this->RemoveFromViewport();
    APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
    if (!ensure(PlayerController!=nullptr))return;
    PlayerController->SetInputMode(FInputModeGameOnly{});
    PlayerController->bShowMouseCursor = false;
}
