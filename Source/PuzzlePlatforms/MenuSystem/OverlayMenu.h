// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "OverlayMenu.generated.h"

/**
 * 
 */
UCLASS()
class PUZZLEPLATFORMS_API UOverlayMenu : public UUserWidget
{
    GENERATED_BODY()
public:
    void SetMenuInterface(class IOverlayMenuInterface* MenuInterface);
    void Setup();
    virtual void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) override;
protected:
    virtual bool Initialize() override;
private:
    IOverlayMenuInterface* OverlayMenuInterface;
    UPROPERTY(meta = (BindWidget))
    class UButton* CloseMenuButton;
    UPROPERTY(meta = (BindWidget))
    class UButton* QuitToMenuButton;
    UPROPERTY(meta = (BindWidget))
    class UButton* QuitToDesktopButton;
    UFUNCTION()
    void CloseClicked();
    UFUNCTION()
    void QuitToMenuClicked();
    UFUNCTION()
    void QuitToDesktopClicked();
    void CloseMenu();
};
