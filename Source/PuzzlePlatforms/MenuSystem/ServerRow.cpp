// Fill out your copyright notice in the Description page of Project Settings.


#include "ServerRow.h"
#include "MainMenu.h"
#include "Components/Button.h"

void UServerRow::Setup(UMainMenu* Parent, uint32 SetupIndex)
{
	this->MainMenu = Parent;
	this->Index = SetupIndex;
}

bool UServerRow::Initialize()
{
	if (!Super::Initialize()) return false;
	if (!ensure(ServerName!=nullptr)) return false;
	if (!ensure(Ping!=nullptr)) return false;
	if (!ensure(HostUsername!=nullptr)) return false;
	if (!ensure(ConnectionFraction!=nullptr)) return false;
	RowButton->OnClicked.AddDynamic(this, &UServerRow::RowSelected);

	return true;
}

void UServerRow::RowSelected()
{
	if (!bIsHeader)
	{
		MainMenu->SelectIndex(Index);
	}
}
