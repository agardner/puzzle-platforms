#pragma once

#include "CoreMinimal.h"

#include "Blueprint/UserWidget.h"
#include "PuzzlePlatforms/PuzzlePlatformsGameInstance.h"
#include "MainMenu.generated.h"
class IMainMenuInterface;

USTRUCT()
struct FServerData
{
	GENERATED_BODY()
	FString Name;
	uint16 CurrentPlayers;
	uint16 MaxPlayers;
	FString HostUsername;
	int32 Ping;
};
UCLASS()
class PUZZLEPLATFORMS_API UMainMenu : public UUserWidget
{
public:
	GENERATED_BODY()
	UMainMenu(const FObjectInitializer& ObjectInitializer);
	void SetMenuInterface(IMainMenuInterface* MenuInterface);
	void Setup();
	virtual void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) override;
	void Teardown();
	void SetServerList(TArray<FServerData> ServerNames);
	void UpdateChildren();
	void SelectIndex(uint32 Index);
protected:
	virtual bool Initialize() override;
private:
	UPROPERTY(meta = (BindWidget))
	class UButton* HostMenuButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* JoinMenuButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* JoinGameButton;

	UPROPERTY(meta = (BindWidget))
	class UPanelWidget* ServerListScrollBox;

	UPROPERTY(meta = (BindWidget))
	class UEditableText* HostServerName;

	UPROPERTY(meta = (BindWidget))
	class UButton* JoinCancelButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* HostCancelButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* RefreshButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* HostGameButton;

	UPROPERTY(meta = (BindWidget))
	class UWidgetSwitcher* MenuSwitcher;

	UPROPERTY(meta = (BindWidget))
	class UWidget* JoinMenu;

	UPROPERTY(meta = (BindWidget))
	class UWidget* MainMenu;

	UPROPERTY(meta = (BindWidget))
	class UWidget* HostMenu;

	UFUNCTION()
	void HostMenuButtonClicked();
	
	UFUNCTION()
	void HostGameButtonClicked();

	UFUNCTION()
	void JoinMenuButtonClicked();

	UFUNCTION()
	void CancelClicked();

	UFUNCTION()
	void RefreshClicked();

	UFUNCTION()
	void JoinServer();
	
	TSubclassOf<UUserWidget> ServerRowClass;
	IMainMenuInterface* MainMenuInterface;
	TOptional<uint32> SelectedIndex;
};
