// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include <Interfaces/OnlineSessionInterface.h>

#include "PuzzlePlatformsGameMode.h"
#include "LobbyGameMode.generated.h"

/**
 * 
 */
UCLASS()
class PUZZLEPLATFORMS_API ALobbyGameMode : public APuzzlePlatformsGameMode {
	GENERATED_BODY()
public:
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exiting) override;
	void StartGame();

	UPROPERTY(BlueprintReadOnly)
	int CountdownSecondsRemaining = 10;
	IOnlineSessionPtr SessionInterface;
private:
	int32 CurrentPlayers;
	FTimerHandle GameStartTimer;
};
