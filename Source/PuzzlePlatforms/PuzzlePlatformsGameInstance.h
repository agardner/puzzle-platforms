#pragma once

#include "CoreMinimal.h"


#include "OnlineSessionSettings.h"
#include "OnlineSubsystem.h"
#include "Engine/GameInstance.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "MenuSystem/MainMenuInterface.h"
#include "MenuSystem/OverlayMenuInterface.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "PuzzlePlatformsGameInstance.generated.h"

class IOnlineSubsystem;
/**
 * 
 */
UCLASS()
class PUZZLEPLATFORMS_API UPuzzlePlatformsGameInstance : public UGameInstance, public IMainMenuInterface,
                                                         public IOverlayMenuInterface {
public:
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable)
	virtual void LoadMainMenu() override;

	UFUNCTION(BlueprintCallable)
	void LoadOverlayMenu();
	UPuzzlePlatformsGameInstance(const FObjectInitializer& ObjectInitializer);
	void StartSession() const; 
protected:
	virtual void Init() override;
	UFUNCTION(Exec)
	virtual void Join(uint32 Index) override;
	UFUNCTION(Exec)
	virtual void Host(FString ServerName) override;
	virtual void RefreshServerList() override;
	virtual void QuitToMainMenu() override;
	virtual void QuitToDesktop() override;
private:
	TSubclassOf<UUserWidget> OverlayMenuClass;
	IOnlineSubsystem* OnlineSubsystem;
	class UOverlayMenu* OverlayMenu;
	TSubclassOf<UUserWidget> MainMenuClass;
	class UMainMenu* MainMenu;
	bool bOverlayOpen = false;
	IOnlineSessionPtr SessionInterface;
	TSharedPtr<class FOnlineSessionSearch> SessionSearch;
	FString DesiredServerName;
	void OnFindSessionsComplete(bool bSuccess);
	void OnCreateSessionComplete(FName Name, bool bSuccess);
	void OnDestroySessionComplete(FName Name, bool bSuccess);
	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);
    void OnNetworkFailure(UWorld* World, UNetDriver* Driver, ENetworkFailure::Type FailureType, const FString& ErrorString);
	void CreateSession() const;
                                                         	void OnNetworkFailure();
};
