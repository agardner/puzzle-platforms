// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyGameMode.h"
#include "PuzzlePlatformsGameInstance.h"
#include "TimerManager.h"

class UPuzzlePlatformsGameInstance;
const static int32 GMax_Players = 3;

void ALobbyGameMode::PostLogin(APlayerController* NewPlayer) {
	Super::PostLogin(NewPlayer);
	++CurrentPlayers;
	UE_LOG(LogTemp, Warning, TEXT("PostLogin: New current players: %d"), CurrentPlayers);
	if (CurrentPlayers >= GMax_Players) {
		GetWorldTimerManager().SetTimer(GameStartTimer, this, &ALobbyGameMode::StartGame, 10);
	}
}

void ALobbyGameMode::Logout(AController* Exiting) {
	Super::Logout(Exiting);
	--CurrentPlayers;
	UE_LOG(LogTemp, Warning, TEXT("Logout: New current players: %d"), CurrentPlayers);
}

void ALobbyGameMode::StartGame() {
	auto* GameInstance = Cast<UPuzzlePlatformsGameInstance>(GetGameInstance());
	if (GameInstance == nullptr) return;
	GameInstance->StartSession();

	GetWorldTimerManager().ClearTimer(GameStartTimer);

	UWorld* World = GetWorld();
	if (!ensure(World!=nullptr)) return;
	bUseSeamlessTravel = true;
	World->ServerTravel("/Game/ThirdPersonCPP/Maps/ThirdPersonExampleMap");
}
